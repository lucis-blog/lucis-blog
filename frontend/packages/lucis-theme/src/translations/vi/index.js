import { header } from "./header"
import { footer } from "./footer"
import { homepage } from "./homepage"
import { postDetail } from "./postDetail"

export const vietnamese = {
  ...header,
  ...footer,
  ...homepage,
  ...postDetail
}