import React from "react"
import { Container } from "../../styles/common"

const Loading = () => {
  return (
    <Container>loading...</Container>
  )
}

export default Loading